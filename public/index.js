/**
 * Created by Daren on 26/4/16.
 */

//Client Side Application

(function() {
    var FilmApp = angular.module("FilmApp", ["ui.router"]);

    var ListCtrl = function($http, $state) {

        var vm = this;
        vm.movielist = [];
        vm.gotoDetails = function(filmId) {
            $state.go("details",
                { film_id: filmId }
            )
        };

        $http.get("/movielist")
            .then(function(result) {
                vm.movielist = result.data;
            }).catch(function(err) {
            console.error(">>error: %s", err);
        });
    };
    
    var DetailsCtrl = function($http, $stateParams, $state) {
        var vm = this;
        vm.movie = {};
        
        $http.get("/movie/" + $stateParams.film_id)
            .then(function(result) {
                vm.movie = result.data
            }).catch(function(err) {
                console.error(">>error: %s", err);
        });
    };
    
    var FilmConfig = function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("listing", {
                url: "/listing",
                templateUrl: "/state/listing.html",
                controller: ["$http", "$state", ListCtrl],
                controllerAs: "listCtrl"
                
            }).state("details", {
                url: "/movie/:film_id",
                templateUrl: "/state/details.html",
                controller: ["$http", "$stateParams", "$state", DetailsCtrl],
                controllerAs: "detailsCtrl"
            });
        
        $urlRouterProvider.otherwise("/listing");
    }

    var FilmCtrl = function() {

    };
    
    FilmApp.config(["$stateProvider", "$urlRouterProvider", FilmConfig]);

    FilmApp.controller("FilmCtrl", [FilmCtrl]);
})();